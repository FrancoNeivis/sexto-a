require('babel-register')({
    presets:['react']
});

//configuramos el servidor
const express = require('express');
const app = express();

const React = require('react');
const ReactDOMServer = require('react-dom/server');

const App = require('./Component.jsx');

//creamos ruta de express
app.get('/',(req, res) =>{

    var html = ReactDOMServer.renderToString(
        React.createElement(App)
    );
    res.send(html);

});


app.listen(3000, ()=>{
    console.log('se esta ejecutando por el puerto 3000');
});