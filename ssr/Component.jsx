const React = require('react');

//creamos nuestro primer componente 
class App extends React.Component{
    handleClick(){
        console.log('Click');
    }
    render() {
        return(
            <div>
                <h1>Contras de renderizar React en el servidor</h1>
                <p>
                    SSR puede mejorar el rendimiento si su aplicación es pequeña.
                    Pero también puede degradar el rendimiento si es pesado.
                    Aumenta el tiempo de respuesta (y puede ser peor si el servidor está ocupado).
                    Aumenta el tamaño de la respuesta, lo que significa que la página tarda más en cargarse.
                    Aumenta la complejidad de la aplicación.
                </p>
                <button onClick={this.handleClick}>Click</button>
            </div>
        )
    }
}

module.exports = App;