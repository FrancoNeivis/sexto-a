const {sum, restar, multiplicar, dividir} = require ('../src/sum')
//const restar = require ('../src/sum')

//utilizaremos describe que nos ayudara crear un 
//bloque que nos ayudara agrupar varias pruebas relacionadas


describe('pruebas matematicas', () =>{
    
    test('Realizamos la multiplicacion', () => {
        expect(multiplicar(5,3)).toBe(15);
    })

    test('Realizamos la dividir', () => {
        expect(dividir(2,2)).toBe(1);
    })
    test('Realizamos la resta', () => {
        expect(restar(1,1)).toBe(0);
    })
})

describe('pruebas para sumas', () => {
    //usamos test que servira para ejecutar la prueba
    test('1 + 2 is 3',() => {
        expect(sum(1,2)).toBe(3)
    })
    it('should return 3 whit 1 + 2', () =>{
        expect(sum(1,2)).toBe(3)
    })

    //prueba con numeros positivos sumando numeros negativos 
    test('1 + (-2) is -1', () =>{
        expect(sum(1, -2)).toBe(-1)
    })

    //prueba con numeros positivos sumando numeros negativos 
    test('-1 + (-2) is -3', () =>{
        expect(sum(-1, -2)).toBe(-3)
    })


    

})
