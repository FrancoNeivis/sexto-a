// toBeNull va hacer matches cuando el valor de algun elemento sea nulo
// toBeUndefined matches cuando un elemento sea indefinido
// toBeDefined cuando se hace una contrapropuesta cuando un elemento si esta definido 
//toBeTruthy matches cuando un valor es verdadero
//toBeFalsy matches cuando un valor sea falso 

test('null', ()=>{
    const n = null

    expect(n).toBeNull()
    expect(n).toBeDefined()
    expect(n).not.toBeUndefined()
    expect(n).not.toBeTruthy()
    expect(n).toBeFalsy()

})

//validaremos un elemento que tenga un valor cero 
test('zero', ()=>{
    const z = 0

    expect(z).not.toBeNull()//esperamos que este elemento no sea null
    expect(z).toBeDefined() //que nuestro elemento este definido
    expect(z).not.toBeTruthy()//zero no sea un elemento un verdadero
    expect(z).toBeFalsy()

})
