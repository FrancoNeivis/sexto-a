//definimos los diferentes matchers comunes 
//usamos matcher toBe NOS PERMITIRA VALIDAR QUE 2 + 2 SEA =4
test('2 + 2 is 4', () =>{
    expect(2 + 2).toBe(4) //usamos para validar 2 elementos
})

//EN CASO DE USAR OBJETOS USAMOS EL MATCHER toEqual 
test('object validation', ()=>{
    const data = {username: 'Neivis-Franco'}
    const data2 = {username: 'Neivis'}

    //usamos espectativas donde un objeto sea igual a otro
    expect(data).toEqual({username: 'Neivis-Franco'})

    //vamos a opener un matcher 
    expect(data).not.toEqual({data2}) 
})


