const { refrescosList, MarcaList } = require('../src/arrays')

describe('Verificar las marcas existentes',() => {
    test('Que el arreglo no sea null', () => {
        expect( MarcaList()).not.toBeNull()
    })

})
//realizamos un comprobacion donde el arreglo no este nulo 

describe('Verificar los refrescos existentes',() => {
    test('Que el arreglo no sea null', () => {
        expect( refrescosList()).not.toBeNull()
    })

    test('Verificar si contiene una cola', () => {
        expect ( refrescosList()).toContain('coca cola')
    })

    test('Comprobar la longitud del arreglo',() => {
        expect( refrescosList()).toHaveLength(4)
    })
})


