
describe('email validation', () => {
    test('Comprobamos dirección de email', () => {
        const email = 'neivis1314@gmail.com'

        expect(email).toMatch(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.([a-zA-Z]{2,4})+$/);
    })

    test('Comprobamos número de teléfono', () => {
       const telefono = '919784852'
        expect(telefono).toMatch(/^[9|6|7][0-9]{8}$/);
    })
})