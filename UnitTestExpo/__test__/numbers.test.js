//matcher para compara numeros o evaluarlos
//toBeGreaterThan()
//toBeGreaterThanOrEqual()
//toBeLessThan()
//toBeLessThanOrEqual()
//toBe()
//toBeCloseTo()
//toEqual()

test ('dos plus dos', ()=>{
    const value = 2 + 2


    expect(value).toBeGreaterThan(3)
    expect(value).toBeGreaterThanOrEqual(3.5)
    expect(value).toBeLessThan(5)//que sean menor que 
    expect(value).toBeLessThanOrEqual(4.5)//que sea menos o igual 
    expect(value).toBe(4)//sea igual 
    expect(value).toEqual(4)//este sera su equivalencia
})

//como validar numeros flotantes

test ('adding floating point number', ()=>{
    const value = 0.1 + 0.2

    expect(value).toBeCloseTo(0.3)
})


