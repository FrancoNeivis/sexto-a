const path = require('path');

module.exports = {
    mode: 'development',

    context: path.resolve(__dirname, 'src/client'),
//especificamos un entrypoin
    entry: {
        app: './index.js',
    },

//output donde se va a mostrar en la carpeta de dist/static
    output: {
        publicPath: '/',
        path: path.resolve(__dirname, 'dist/static'),
        filename: 'bundle.js'
    },

//configuramos el babel-loader de nuestra aplicacion
    module: {
        rules: [
            {
                test: /\.js/,
                exclude: /node_modules/,
                loader: 'babel-loader'
            }
        ],
    },

//agregamos source-map
    devtool: 'inline-source-map',
};