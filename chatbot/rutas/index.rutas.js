const express = require('express');
const router = express.Router();
const Producto = require("../models/productos.model")
const Usuario = require("../models/usuarios.model")

//PRODUCTOS
//GET obtener todos los productos
router.get('/productos', (req, res) => {
    Producto.find((err,productos)=>{
        if(err) return res.status(500).send({message:`Error al realizar la consulta ${err}`}) 
        res.status(200).send({productos})
    });
    
});

//GET INDIVIDUAL obtener productos por id
router.get('/productos/:productoId', (req, res) => {
    let productoId = req.params.productoId

    Producto.findById(productoId,(err,productos)=>{
        if (err) return res.status(500).send({message:`Error al realizar la consulta ${err}`})   
        if(!productos) return res.status(404).send({mesage:`Producto no encontrado`});
        res.status(200).send({productos})
    })
});

//POST agregar nuevos productos a la base de datos
router.post('/productos', (req,res)=>{
    let producto = new Producto ({
        nombre:req.body.nombre,
        descripcion:req.body.descripcion,
        categoria:req.body.categoria,
        cantidad:req.body.cantidad,
        precio:req.body.precio,
        img:req.body.img,
    });
    producto.save(()=>{
        
        res.json({"Mensaje":"Producto almacenado sin problemas!"});
        //if(error) return res.json({ok:false,msg:"Hubo un error"})
    })  
});

//PUT Actualizar productos
router.put('/productos/:productoId', (req, res) => {
    let productoId = req.params.productoId;
    let update=req.body;
    Producto.findByIdAndUpdate(productoId,update,(err,productoUpdated)=>{
        if (err)  res.status(500).send({message:`Producto no encontrado ${err}`}) 
    res.status(200).send({ productos: productoUpdated })
    })
});

//DELETE Eliminar productos por id
router.delete('/productos/:productoId', (req, res) => {
    let productoId = req.params.productoId
    Producto.findById(productoId,(err,productos)=>{
        if (err)  res.status(500).send({message:`Producto no encontrado ${err}`})   
    productos.remove(err=>{
        if (err) res.status(500).send({message:`Error al eiminar el producto ${err}`})   
        res.status(200).send({message:`El producto fue eliminado correctamente`})
    })
    }) 
    
});

//USUARIOS
//GET Obtener todos usuarios
router.get('/usuarios', (req, res) => {
    Usuario.find((err,usuarios)=>{
        if(err) return res.status(500).send({message:`Error al realizar la consulta ${err}`}) 
        res.status(200).send({usuarios})
    });
    
});

//GET INDIVIDUAL obtener usuarios por id
router.get('/usuarios/:usuarioId', (req, res) => {
    let usuarioId = req.params.usuarioId

    Usuario.findById(usuarioId,(err,usuarios)=>{
        if (err) return res.status(500).send({message:`Error al realizar la consulta ${err}`})   
        if(!usuarios) return res.status(404).send({mesage:`Usuario no encontrado`});
        res.status(200).send({usuarios})
    })
});

//POST agregar nuevos usuarios a la base de datos
router.post('/usuarios', (req,res)=>{
    let usuario = new Usuario ({
        cedula: req.body.cedula,
        nombre: req.body.nombre,
        apellido: req.body.apellido,
        correo: req.body.correo,
        contraseña:req.body.contraseña
    });
    usuario.save(()=>{
        //if(error) return res.json({ok:false,msg:"Hubo un error"});
        res.json({"Mensaje":"Usuario guardado sin problemas!"});
    })
});

//PUT Actualizar productos
router.put('/usuarios/:usuarioId', (req, res) => {
    let usuarioId = req.params.usuarioId;
    let update=req.body;
    Usuario.findByIdAndUpdate(usuarioId,update,(err,usuarioUpdated)=>{
        if (err)  res.status(500).send({message:`Usuario no encontrado ${err}`}) 
    res.status(200).send({ usuarios: usuarioUpdated })
    })
});

//DELETE Eliminar productos por id
router.delete('/usuarios/:usuarioId', (req, res) => {
    let usuarioId = req.params.usuarioId
    Usuario.findById(usuarioId,(err,usuarios)=>{
        if (err)  res.status(500).send({message:`Usuario no encontrado ${err}`})   
    usuarios.remove(err=>{
        if (err) res.status(500).send({message:`Error al eiminar el usuario ${err}`})   
        res.status(200).send({message:`El usuario fue eliminado correctamente`})
    })
    })
}) 



module.exports = router;