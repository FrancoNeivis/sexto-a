const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require('mongoose');


const { MONGO_URI } = require('./config')
const app = express();

const port = process.env.PORT || 4000;

//conectarnos a nuestra base de datos
mongoose.connect(MONGO_URI, { useNewUrlParser:true, useUnifiedTopology:true })
.then(p=>{
    console.log('Conexión exitosa')
}).catch(err=>{
    console.log('Error de conexión',err);
}) ;


// for parsing json
app.use(
  bodyParser.json({
    limit: "20mb",
  })
);
// parse application/x-www-form-urlencoded
app.use(
  bodyParser.urlencoded({
    extended: false,
    limit: "20mb",
  })
);
app.use('/public', express.static(__dirname+'/public'));
app.use(express.json());
app.use("/",require("./rutas/index.rutas"))
app.use("/messenger", require("./Facebook/facebookBot"));

app.get("/", (req, res) => {
  return res.send("Chatbot de Megamarket NEM Funcionando");
});

app.listen(port, () => {
  console.log(`El servidor está escuchando por el puerto ${port}`);
});
