let mongoose = require('mongoose');
let Schema = mongoose.Schema;

//cliente schema definition
let UserSchema = new Schema(
  {
    nombre: { type: String, required: true },
    apellido: { type: String, required: true },
    telefono: { type: String, required: true },
    direccion: { type: String, required: true },
    //createdAt: { type: Date, default: Date.now },
  },
  {      
    //versionKey: false,
    timestamps: true
  }
);

// Sets the createdAt parameter equal to the current time 
UserSchema.pre('save', next => {
  now = new Date();
  if(!this.createdAt) {
    this.createdAt = now;
  }
  next();
});

//Exports the ClienteSchema for use elsewhere.
module.exports = mongoose.model('user', UserSchema);