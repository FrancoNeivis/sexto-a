const express = require('express') ;
const app = express();


const response =
{
    data:[],
    services:"Product services",
    architecture:"Microservices"
}

const logger = message=> console.log(`Mensaje desde Product service:  ${message}`)

app.use((req,res,next)=>{
    response.data=[];
    next();
})

//url http://localhost:3000/api/v2/products

app.get("/api/v2/products", (req,res)=>{
    response.data.push(
        "Leche Entera",
        "Deja",
        "Arroz"
    );
    logger("Get data products");
    return res.send(response);
})

module.exports= app;
