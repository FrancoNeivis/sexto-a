const express = require('express') ;
const app = express();


const response =
{
    data:[],
    services:"Orden services",
    architecture:"Microservices"
}

const logger = message=> console.log(`Mensaje desde Orden service:  ${message}`)

app.use((req,res,next)=>{
    response.data=[];
    next();
})

//url http://localhost:3000/api/v2/ordenes

app.get("/api/v2/ordenes", (req,res)=>{
    response.data.push(
        "pedido1",
        "pedido2",
        "pedidos3"
    );
    logger("Get data ordenes");
    return res.send(response);
})

module.exports= app;
