const express= require('express');
const app = express();

const response =
{
    data:[],
    services: "Monolithic service",
    architecture: "Monolithic"
}

//get  api/v1/
app.use((req, res, next)=>{
    response.data=[];
    next();
})

//get usuario http://localhost:3000/api/v1/users
app.get('api/v1/users',(req,res)=>{
    response.data.push('Administrador', 'Inventario', 'Neivis');
    return res.send(response)

})

//get libros api/v1/products
app.get('api/v1/products',(req,res)=>{
    response.data.push(
        "Leche Entera",
        "Deja",
        "Arroz"
    );
    return res.send(response)

})

//get carros api/v1/orden
app.get('api/v1/orden',(req,res)=>{
    response.data.push(
        "pedido1",
        "pedido2",
        "pedidos3"
    );
    return res.send(response)

})


module.exports = app;