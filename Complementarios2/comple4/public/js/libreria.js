var URLRest='http://localhost:5000/v1/api/user';
window.addEventListener('load', function()
{

    let htmlGenerado='';

    htmlGenerado+=`<label for="txtid">ID</label>`;
    htmlGenerado+=`<input type="text" name="" id="txtid">`;
    htmlGenerado+=`<label for="txtname">NAME</label>`;
    htmlGenerado+=`<input type="text" name="" id="txtname">`;
    htmlGenerado+=`<label for="txtusername">USER NAME</label>`;
    htmlGenerado+=`<input type="text" name="" id="txtusername">`;
    htmlGenerado+=`<label for="txtpassword">PASSWORD</label>`;
    htmlGenerado+=`<input type="text" name="" id="txtpassword">`;
    htmlGenerado+=`<label for="txtTipoUser">TIPO </label>`
    htmlGenerado+=`<select name='selectUsuario' id='txtTipoUser' name=''> <option value='Administrador'>Administrador</option> <option value='Invitado'>Invitado</option> <option value='Gestion'>Gestión</option></select> `
    
    htmlGenerado+=`<button id="btnnuevo">NUEVO</button>`;
    htmlGenerado+=`<button id="btnconsultar">CONSULTAR</button>`;
    htmlGenerado+=`<button id="btngrabar">GRABAR</button>`;
    htmlGenerado+=`<button id="btnmodificar">MODIFICAR</button>`;
    htmlGenerado+=`<button id="btneliminar">ELIMINAR</button>`;
    htmlGenerado+=`<div id="divcontenido"></div>`;

    htmlCuerpo.innerHTML=htmlGenerado;
    
    //Funcionalidades

    //Nuevo
    btnnuevo.addEventListener('click',function(){
        txtid.value='';
        txtname.value='';
        txtusername.value='';
        txtpassword.value='';
        txtTipoUser.value='';
    });

    //Consultar
    btnconsultar.addEventListener('click',function(){
        fetch(URLRest).then(resultado=>{
            return resultado.json()
        })
        .then(consulta=>{
            let htmlGenerado=`<table border=1`;
            for (const indice in consulta)
            {
                htmlGenerado+=`<tr>`
                let elemento = consulta[indice];
                htmlGenerado+=`<td> ${elemento.name} </td> <td>${elemento.password} </td><td>${elemento.tipoUser} </td> <td> <button class='consultai' value= '${elemento._id}' >${elemento.username} </button> </td> `;
                htmlGenerado+=`</tr>`
            }
            divcontenido.innerHTML=htmlGenerado;
            //asignacion del boton a la consulta individual
            document.querySelectorAll(`.consultai`).forEach(elemento=>{
                elemento.addEventListener('click',function(){
                    consultaindividual(elemento.value);
                })
            })
        })
    });

    //Consulta Individual
    function consultaindividual(parametro)
    {
        var url = `${URLRest}/${parametro}`;
        fetch(url).then(resultado=>{
            return resultado.json()
        }).then(respuesta=>{
            txtid.value=respuesta._id;
            txtname.value=respuesta.name;
            txtusername.value=respuesta.username;
            txtpassword.value=respuesta.password;
            txtTipoUser.value=respuesta.tipoUser;
        })
    };

    //Grabar
    btngrabar.addEventListener('click',function(){
        var data = {name:txtname.value, username:txtusername.value, password:txtpassword.value, tipoUser:txtTipoUser.value}
        fetch(
            URLRest,
            {
                method:'POST',
                body: JSON.stringify(data),
                headers:{ //Indicar tipo de datos con el que se va a trabajar
                    'Content-type':'application/json'
                }
            }
        ).then(respuesta=>{
            return respuesta.json()
        })
        .then(resultado=>{
            console.log(`El usuario con nombre ${resultado.name} y el id ${resultado._id}se almacenó correctamente`)
        }).catch(error=>{
            console.log(`Error`, error)
        })
    });

    //Modificar
    btnmodificar.addEventListener('click',function(){
        var data = {name:txtname.value, username:txtusername.value, password:txtpassword.value, tipoUser:txtTipoUser.value}
        fetch(
            `${URLRest}/${txtid.value}`,
            {
                method:'PATCH',
                body: JSON.stringify(data),
                headers:{ //Indicar tipo de datos con el que se va a trabajar
                    'Content-type':'application/json'
                }
            }
        ).then(respuesta=>{
            return respuesta.json()
        })
        .then(resultado=>{
            console.log(`El usuario con nombre ${resultado.name} y el id ${resultado._id}se modificó correctamente`)
        }).catch(error=>{
            console.log(`Error`, error)
        })
    })

    //Eliminar
    btneliminar.addEventListener('click',function(){
        var url = `${URLRest}/${txtid.value}`;
        fetch(url,{
            method:'DELETE'
        }).then(respuesta=>{
            return respuesta.json()
        }).then(resultado=>{
            console.log(resultado)
        }).catch(error=>{
            console.log(`Error`, error)
        })
    })
})