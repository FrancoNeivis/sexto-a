const request = require('supertest')
const app = require('../api')
const { nanoid } = require('nanoid')

//setup de pruebas en express
//arranca el servidor de prueba
let testServer
beforeAll(() => {
    testServer = app.listen(4000)
})
//finalice el servidor 
afterAll((done) => {
     testServer.close(done)
})

//listar a todos los usuarios que este en la base de datos 
describe('GET /api/users', () => {
    it ('should return all users', async () => {
        const response = await request(app).get('/api/users')

        expect(response.error).toBe(false)
        expect(response.status).toBe(200)
        expect(response.body.body).not.toBeNull()
        expect(Array.isArray(response.body.body)).toBe(true)
        expect(response.body.body.length).toBe(2)
    })
})

//obtener un usuario
describe('GET /api/users/:id', () => {
    it ('should GET a exercise', async () => {
        const response = await request(app).get('/api/users/1')
        expect(response.error).toBe(false)
        expect(response.status).toBe(200)
        expect(response.body.body).not.toBeNull()
        expect(response.body.body.id).toBe("1")
    })
})

//insertar un usuario
describe('POST /api/users', () => {
    it ('should POST a new user', async () => {
        const userId = nanoid()
        let user = {
            id: userId,
            name: 'Nuevo usuario',
            username: 'neivis-franco'
        }
        const response = await request(app).post('/api/users').send(user)
        expect(response.error).toBe(false)
        expect(response.status).toBe(200)
        expect(response.body.body).not.toBeNull()
        expect(response.body.body.id).toBe(userId)
    })
})
