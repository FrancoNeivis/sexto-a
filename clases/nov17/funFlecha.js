//funcion flecha
const saludar =(nombre) => {
    console.log(`Hola ${nombre}`);
}

saludar('neivis')

//funciones flechas mas rapidas para ser definidas, mas elegante
//mas utilizadas de forma anonimas, en especial para trabajar con colecciones
//las funciones errow no tienen contexto, por aquello no existe THIS, es decir 
//su contexto es segun quien la invoca 
//las funciones flacha son siempre definidas como expresiones, es decir 
//deben definirse previo a su uso.
