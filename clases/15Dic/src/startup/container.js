const { creatContainer, asClass, asFuncion, asValue } = require('awilix');

//config
const config = require('../config');
//sartup
const app = require('api');
//services 
const { HomeService } = require('../services')

//controllers

//routes

const { HomeRoutes } = require('../routes/index.routes');
const Routes = require('../routes')



const container = creatContainer();


container.register(
    {
        app: asClass (app).singleton(),
        router: asFunction(Routes).singleton(),
        config: asValue(config)
    }
).register(
    {
        HomeService: asClass(HomeService).singleton()
    }
).register(
    {
        HomeController: asClass(HomeController.bind(HomeController)).singleton()
    }
)
.register(
    {
        HomeRouter: asFuncion(HomeRoutes).singleton()
    }
)
module.exports = container;