 const { Router } = require('express');

 module.exports = function({HomeContraller}){
     const router = Router();
     router.get('/', HomeContraller.index);
     return router;
 }
