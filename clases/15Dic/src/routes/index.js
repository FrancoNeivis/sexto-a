
const express = require('express')
const cors = require ('cors')
const helmet = require('helmet');
const compression = require ('compression');
const { request, Router } = require('express');
const { HomeRoutes } = require('./index.routes');
require('express-async-errors')


module.exports = function(){
    const routes = express.Router();
    const apiRouter = express.Router();

    apiRouter.use(express.json())
    .use(cors())
    .use(helmet())
    .use(compression());

    apiRouter.use('/home', HomeRoutes);

    routes.use('/v1/api', apiRouter);

    //

    return Router;
}
