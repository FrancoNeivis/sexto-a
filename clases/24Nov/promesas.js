//FUNCION CALLBACK  

//definir una persistencia 
// se definen (arreglos, base de datos, servicios web monoliticos, micro)
const libros=[
    {
        id:1,
        titulo:'Aprendamos PHP como en 1990',
        idautor:1
    },
    {
        id:2,
        titulo:'PHP apara expertos',
        idautor:2
    },
    {
        id:3,
        titulo:'JavaScrpt para principiantes',
        idautor:2
    }
]


const autores =[
    {
        id:1,
        nombre:'Neivis Franco'
    },
    {
        id:2,
        nombre:'Dayanara Reyes'
    }
]

//----------APLICAR PROMESAS---------------

//ELIMINAMOS CALLBACK ----
function buscarLibroPorId(id)
{
    //Devolvera una promesas
    return new Promise((resolve, reject) =>{

        const libro = libros.find(
            //hacemos uso de una funcion fee y buscamos el parametro que necesitamos
            (libro)=>
            {
                return libro.id === id; 
            }
        )
    
       
        if(!libro)//si el autor no existe
        {
            const error = new Error();
            error. message="El libro no existe !! "
        
            reject(error);
        
        }
        resolve(libro);
        
    } )
   
}

//funcion que busca autores por ID
function buscarAutorPorId(id, callback)
{
  return new Promise((resolve, reject)=>{
    const autor = autores.find(
        //hacemos uso de una funcion fee y buscamos el parametro que necesitamos
        (autor)=>
        {
            return autor.id === id; 
        }
    )

    if(!autor)
    {
        const error = new Error();
        error. message="El libro no existe !! "
        reject(error);
        
    }
    resolve(autor);
    
  })
    
}

let libroAuxiliar={};
//promesas encadenas primero un dato y luego el otro
buscarLibroPorId(3).then(libro=>{
    libroAuxiliar=libro;
    return buscarLibroPorId(libro.idautor);
})
.then(autor =>{
    libroAuxiliar.autor=autor;
    delete libroAuxiliar.idautor;
    console.log(libroAuxiliar)
}).catch(err=>{
    console.log(err.message);
})



