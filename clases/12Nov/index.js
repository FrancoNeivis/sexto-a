/*------definicion de variables----
var prueba="hola "
let prueba2="chao"
const prueba3="ok"       valores constante como IVA 
*/

/*-----------TIPOS DE DATOS PARA VARIABLES----------

Object
String
Number
Boolean
Array 
Set
Symbol
Function
unidefined     no esta definido
null           no tienen valores */


/*---------------CONDICIONES-------------
const calificacion=68


if (calificacion>=90)
{
console.log(`A`);
}
else if (calificacion>=80 && calificacion<90)  se debe utilizar else para mejorar el rendimiento
{
    console.log(`B`)
}
else 
{
     console.log(`Reprobado`)
}
*/


/*------------- como usar un switch case------------*/


const n1=80;
const n2=90;


const operacion="multiplicar" //si quiero que sume o reste debo cambiar de operacion


switch (operacion)
{
    case "sumar":
        console.log(n1+n2);
        break;  //hace el papel del else  donde termina el flujo del programa 
    case "restar":
        console.log(n1-n2);
        break;
    case "multiplicar":
        console.log(n1*n2);
        break;
    default:
        console.log(`Esta operación no se encuentra definida`);


}
