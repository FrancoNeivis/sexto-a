//libreria instaladas
const mongoose = require('mongoose');
const axios= require('axios').default;
const cheerio=require('cheerio')
const cron=require('node-cron')

//componentes locales
const { MONGO_URI} = require('./config')
const { Noticias} = require('./models')

//conectarnos a nuestra base de datos ORM servira para explorar los datos 
mongoose.connect(MONGO_URI, { useNewUrlParser: true, useUnifiedTopology:true});
/*.then(p=>{
    console.log('conexion exitosa')
    }).catch(err=>{
        console.log(err);
    })*/

//defenir la periodicidad utilizando CRON JOB
cron.schedule("* * * * *", async()=>{


    //1. aceder a nuestra URL de CNN para tomar el HTML
    const html= await axios.get("https://cnnespanol.cnn.com/")
    
    //2. Procesar ese HTML utlizamos cheerio y recorrer las noticias
    const $ = cheerio.load(html.data);
    //obteniendo todos los elementos que pertenecen a la  clase new_title
 
    const titulos = $(".news_title");
    

    titulos.each((index, element)=>{
      
        //console.log         //3. almacenamos la informacion en mongodb utlizando Mongodb
        const noticia= {
            titulo: $(element) .text().toString(),
            enlace: $(element).children().attr("href")
        }
        Noticias.create([noticia]);
    })
   
    console.log('Proceso terminado')

})

