//PREGUNTO SI SE ESTA EJECUTANDO MODO PRODUCCION
if (process.env.NODE_ENV !== "production")
{
    //CONFIGURACION 
    require('dotenv').config()
}

//exponer una libreria, variable, o expancion
//exponer el uri

module.exports = {
    MONGO_URI : process.env.MONGO_URI
}
