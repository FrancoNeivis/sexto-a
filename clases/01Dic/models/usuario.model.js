const mongoose = require('mongoose');

//indicar que vamos a trabajr con esquemas
const {Schema } = mongoose

const UsuariosSchema = new Schema (
    {
        Nombre: {type:String},
        Apellidos: {type:String}
    },
    //marca de tiempo 
    {timestamps: { createdAt: true, updateAt:true }}
)

module.exports = mongoose.model("Usuarios", UsuariosSchema);