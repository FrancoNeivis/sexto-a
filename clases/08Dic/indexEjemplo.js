//para tener una buena estructura para hacer autenticacion 
//usamos express

const fs = require('fs');
const express = require('express');
const { Server } = require('http');

const path = require('path');

const PUERTO=3000;
const index = fs.readFileSync('./index.html');
const about = fs.readFileSync('./about.html');


//definimos un servidor en express
const server = express();


var paginaDeError = path.join( __dirname, "./error.html")


/*todo lo que llamamos por el url use el /
utilzamos un funcion normal y llamandola en linea*/
server.get("/", devolverIndex)


/*utilizamos una funcion flecha
/si queremos usar otra ruta solo copiamos y cambiamosla ruta*/
server.get("/about", (req,res,next)=>{
    res.write(about)
})
//anteriormente bodyparser,ahora viene en el core de NODE
server.use(express.json())


//------------------------para que escuche los metodos 
server.listen (PUERTO, ()=>{
    console.log(`Servidor esta ejecutandose por el puerto ${PUERTO}`)
})

//definimos un MIDDLEWARE muy basico
server.use((req, res,next)=>{
    res.status(404).sendFile(paginaDeError)
})


//----------------------tambien podriamos usar en una funcion 
function devolverIndex(req, res)
{
    res.write(index)
}


/*const fs = require('fs')
const http = require('http')


const index = fs.readFileSync('./index.html');
const about = fs.readFileSync('./about.html');



http.createServer((request,response)=>{
    const { url } = request;
    if (url==='/')
    {
        response.writeHead(200, {"Content-type":"text/html"});
        response.write(index);

    }
    else if (url==='/about')
    {
        response.writeHead(200, {"Content-type":"text/html"});
        response.write(about);

    }
    else
    {
        response.writeHead(404, {"Content-type":"text/html"});
        response.write("Not found!!");
    }

})
.listen(3000, ()=>{
    console.log('Servidor corriendo')
})
*/