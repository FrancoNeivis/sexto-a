//FUNCION CALLBACK  

//definir una persistencia 
// se definen (arreglos, base de datos, servicios web monoliticos, micro)
const libros=[
    {
        id:1,
        titulo:'Aprendamos PHP como en 1990',
        idautor:1
    },
    {
        id:2,
        titulo:'PHP apara expertos',
        idautor:2
    },
    {
        id:3,
        titulo:'JavaScrpt para principiantes',
        idautor:1
    }
]


const autores =[
    {
        id:1,
        nombre:'Neivis Franco'
    },
    {
        id:2,
        nombre:'Dayanara Reyes'
    }
]



//aplicaremos un callback en la funcion
function buscarLibroPorId(id, callback)
{
    //porque usamos ===
    //123 == "123" lengua tipado comparar valores
    //123 === "123" ya no sera verdadera si no falsa compara valores y tipo de datos 


    
    //buscamos en el arreglo libro uno que tenga ese id
    const libro = libros.find(
        //hacemos uso de una funcion fee y buscamos el parametro que necesitamos
        (libro)=>
        {
            return libro.id === id; 
        }
    )

    //como pregunto si el libro existe para comprovar si el dato es valido o no 
    // si no existe generamos un error y lo enviamos al callback
    if(!libro)//si el autor no existe
    {
        const error = new Error();
        error. message="El libro no existe !! "
    
        //devolvemos un resultado
        //usando un callback
        //como primer parametro seria un error (eseccion)algo que no esta controlado
        return callback(error);

    }
    //si no se exite error debemos enviar el libro al callback  enviamos primero null
    callback(null, libro);
    
}

//funcion que busca autores por ID
function buscarAutorPorId(id, callback)
{
  
    const autor = autores.find(
        //hacemos uso de una funcion fee y buscamos el parametro que necesitamos
        (autor)=>
        {
            return autor.id === id; 
        }
    )

    if(!autor)
    {
        const error = new Error();
        error. message="El libro no existe !! "
    
        return callback(error);
    }
    callback(null, autor);
    
}



//----------------CALLBACK HELL--------------
//numero de libro que quiero buscar y recibira como paramentro un error o un libro 
buscarLibroPorId(2, (err, libro)=>{
    //existe o no existe un error 
    if(err)
    {
        return console.log(err.message)
    }
    buscarAutorPorId( libro.idautor, (err,autor)=>{
        if (err)
        {
            return console.log(err.message);
        }
        //MUESTRA LOS DATOS DEL LIBRO CON EL AUTOR 
        console.log(`El libro ${libro.titulo} fue escrito por ${autor.nombre}`);
    })
    

})






