const mongoose = require('mongoose');
const express = require('express');
const { MONGO_URI} = require('./config')
const bodyParse = require('body-parser');
const Product = require('./app/routes/product');
const app =  express();

const PORT = 3000;

//nos conectamos a la base de datos 
mongoose.connect(MONGO_URI, { useNewUrlParser:true, useUnifiedTopology:true })
.then(p=>{
    console.log('Conexion a base de datos exitosa')
}).catch(err=>{
    console.log('Error de conexión',err);
}) ;

app.use(bodyParse.json());


app.use('/product', Product);




app.listen(PORT, ()=>{
    console.log(`Servidor se esta ejecutando por el puerto ${PORT}`)
})




