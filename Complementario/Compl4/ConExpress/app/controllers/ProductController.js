const Product = require('../models/Product');



// funcion para listar el producto
function index (req, res){
    Product.find({})
        .then(products =>{
            if(products.length) return res.status(200).send({products});
            return res.status(204).send({message: 'NO CONTENT'});
        }).catch(err => res.status(500).send({err}))

}
// funcion para mostar uno o especificos el producto
function show(req,res){
    if(req.body.err) return res.status(500).send({err});
    if(!req.body.products) return res.status(404).send({message: 'NOT FOUND'});
    let products = req.body.products;
    return res.status(200).send({products});

}
// funcion para crear el producto
function create (req, res){
    let product =new Product(req.body);
    product.save().then(product => res.status(201).send({product})).catch(err => res.status(500).send({err}));

}
// funcion para actualizar el producto
function update(req, res){
    if(req.body.err) return res.status(500).send({err});
    if(!req.body.products) return res.status(404).send({message: 'NOT FOUND'});
    let product = req.body.products[0];
    product = Object.assign(product,req.body);
    product.save().then(product => res.status(200).send({message: 'UPDATE', product})).catch(err => res.status(500).send({err}));
    
}
// funcion para eliminar el producto
function remove(req, res){
    if(req.body.err) return res.status(500).send({err}); 
    if(!req.body.products) return res.status(404).send({message: 'NOT FOUND'});
    req.body.products[0].remove().then(product => res.status(200).send({mesage: 'REMOVED', product})).catch(err => res.status(500).send({err}));
}


function find(req,res,next){
    let query = {};
    query[req.params.key] = req.params.value;
    Product.find(query).then(products => {
        if(!products.length) return next();
        req.body.products = products;
        return next();
    }).catch(err =>{
        req.body.err = err;
        next();
    })
}

module.exports = {
    index,
    show,
    create,
    update,
    remove,
    find
}