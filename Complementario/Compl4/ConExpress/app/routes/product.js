//definir cuales son las direcciones a las que 
//puede acceder nuestra API 

const express = require('express');
const ProductCtrl = require ('../controllers/ProductController');

const Router = express.Router();

Router.get('/', ProductCtrl.index)  // ConExpress.com/product/ listar todos los productos 
      .post('/',ProductCtrl.create)    // ConExpress.com/product/    craer un nuevo producto
      .get('/:key/:value',ProductCtrl.find,ProductCtrl.show)     // ConExpress.com/product/category/Bebidas show muestra producto en especifico
      .put('/:key/:value', ProductCtrl.find,ProductCtrl.update)     // ConExpress.com/product/name/AbsolutVodka  Actualiza un producto en especifico
      .delete('/:key/:value',ProductCtrl.find,ProductCtrl.remove); // ConExpress.com/product/name/AbsolutVodka


module.exports = Router;