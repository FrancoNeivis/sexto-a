//trabajaremos con 2 librerias 
const fs = require('fs')
const http = require('http')

//primero debemos leer el contenido de la pagina 
const index = fs.readFileSync('./index.html');
const about = fs.readFileSync('./about.html');


//creamos un servidor de forma manual
http.createServer((request,response)=>{
    const { url } = request;
    if (url==='/index')
    {
        response.writeHead(200, {"Content-type":"text/html"});
        response.write(index);

    }
    else if (url==='/about')
    {
        response.writeHead(200, {"Content-type":"text/html"});
        response.write(about);

    }
    else
    {
        response.writeHead(404, {"Content-type":"text/html"});
        response.write("Not found!!");
    }
    
//levantamos el servidor
}).listen(3000, ()=>{
    console.log('Servidor corriendo')
})

