const mongoose = require('mongoose');

//indicar que vamos a trabajr con esquemas
const {Schema } = mongoose

const NoticiasSchema = new Schema (
    {
        titulo: {type:String},
        enlace: {type:String}
    },
    //marca de tiempo 
    {timestamps: { createdAt: true, updateAt:true }}
)

module.exports = mongoose.model("Noticia", NoticiasSchema);